var util = require('../../utils/util.js')
var app = getApp()

var config = {
    data: {
      highscore44:0,
      highscore55: 0,
      rank44:0,
      rank55: 0,

      userInfo: {},
      hasUserInfo: false,
      currentRankType:'4*4排行榜',
      canIUse: wx.canIUse('button.open-type.getUserInfo'),

        modalHidden: true,
        rankTypes: ['4*4排行榜', '5*5排行榜'],
        rankInfo4x4: [
          {  name: 'debuggor', score: 180000, img: 'rank1.png' }, 
          { name: '慢慢结冰', score: 163148, img: 'rank2.png' }, 
          { name: '无名小卒', score: 146088, img: 'rank3.png' },
          { name: '清风', score: 136024, img: 'rank4.png' },
          { name: 'L~M。L', score: 122908, img: 'rank5.png' },
          {  name: '叶子', score: 115283, img: 'rank6.png' }
          ],

        rankInfo5x5: [
          { name: '丫丫', score: 580000, img: 'rank1.png' },
          { name: '我是一只猫', score: 563148, img: 'rank2.png' },
          { name: '黄彬', score: 546088, img: 'rank3.png' },
          { name: '鬼迷心窍', score: 536024, img: 'rank4.png' },
          { name: 'Vincy', score: 522908, img: 'rank5.png' },
          { name: 'zhaihai', score: 515283, img: 'rank6.png' }
          
        ]
    },
    changeRankType: function (e) {
      var rankType = e.currentTarget.dataset.rankType
      this.setData({
        currentRankType: rankType
      })
    },

    onLoad: function () {
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
      this.hightScore()
    },

    getUserInfo: function (e) {
      console.log(e)
      app.globalData.userInfo = e.detail.userInfo
      this.setData({
        userInfo: e.detail.userInfo,
        hasUserInfo: true
      })
    },
//---------------
    hightScore(){
      wx.getStorage({
        key: 'highscore44',
        success: res => {
          let highscore44 = res.data
          this.setData({
            highscore44: highscore44,
            rank44: highscore44 > 0 ? Math.abs(Math.ceil((-7 / 100) * highscore44 + 19324)) : 0
          })
        }
      })
      wx.getStorage({
        key: 'highscore55',
        success: res => {
          let highscore55 = res.data
          this.setData({
            highscore55: highscore55,
            rank55: highscore55 > 0 ? Math.abs(Math.ceil((-3 / 100) * highscore55 + 59324)) : 0
          })
        }
      })
    },
//---------------
  /**
 * 用户点击右上角分享
 */
    onShareAppMessage: function () {
      return {
        title: '诚邀你一起来挑战2048排行！',
        path: '/pages/index/index',
      }
    }
}

Page(config);
